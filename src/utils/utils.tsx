import { loginRequest } from '../config/authConfig';
import { graphConfig } from '../config/authConfig';

/**
 * A wrapper for calling MS Graph with Auth Token
 * Hint - this still neds to be refined with better doc and typings
 * @function
 * @template A
 * @param {Object} instance
 * @param {Object} accounts
 * @param {Object} params
 * @returns {Promise<A>}
 */
export async function loadMSData(instance: any, accounts: any, { url, method } = {} as any) {
  // const { instance, accounts } = useMsal();
  try {
    const accessToken = localStorage.getItem('token');
    if (!accessToken) {
      await setupAccessToken(instance, accounts);
    }
    const resp = await callMsGraph({ url, method });
    if (Object.prototype.hasOwnProperty.call(resp, 'error')) {
      throw Error;
    }
    return resp;
  } catch (e) {
    const request = { ...loginRequest, account: accounts[0] };
    await instance.acquireTokenPopup(request);
    await setupAccessToken(instance, accounts);
    return await callMsGraph({ url, method });
  }
}

// Silently acquires an access token which is then attached to a request for Microsoft Graph data
async function setupAccessToken(instance: any, accounts: any) {
  const request = {
    ...loginRequest,
    account: accounts[0]
  };
  const response = await instance.acquireTokenSilent(request);
  localStorage.setItem('token', response.accessToken);
}

/**
 * Attaches a given access token to a Microsoft Graph API call. Returns information about the user
 */
const params = {
  url: graphConfig.graphMeEndpoint,
  method: 'GET'
};

export async function callMsGraph({ url, method } = params) {
  const accessToken = localStorage.getItem('token');
  const headers = new Headers();
  const bearer = `Bearer ${accessToken}`;
  headers.append('Authorization', bearer);

  const options = {
    method: method,
    headers: headers
  };

  try {
    const response = await fetch(url, options);
    const data = await response.json();
    return data;
  } catch (error) {
    console.log(error);
  }
}
