import { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import MyEvents from './components/MyEvents';
import RoomEvents from './components/RoomEvents';
import { AuthenticatedTemplate, UnauthenticatedTemplate, useMsal } from '@azure/msal-react';
import { ProfileData } from './components/ProfileData';
import SignInButton from './components/SignInButton';
import { loadMSData } from './utils/utils';

function ProfileContent() {
  const { instance, accounts } = useMsal();
  const [graphData, setGraphData] = useState(null);
  const name = accounts[0] && accounts[0].name;

  async function RequestProfileData() {
    const resp = await loadMSData(instance, accounts);
    setGraphData(resp);
  }

  return (
    <>
      <h5 className="card-title">Welcome {name}</h5>
      {graphData ? (
        <ProfileData graphData={graphData} />
      ) : (
        <button type="button" onClick={RequestProfileData}>
          Request Profile Information
        </button>
      )}
    </>
  );
}

function App() {
  return (
    <div className="App">
      <img src={logo} className="App-logo" alt="logo" />
      <AuthenticatedTemplate>
        <p>
          You are signed in!
          <br />
          Hello Vite + React!
        </p>
        <ProfileContent />

        <div className="eventsWrapper">
          <MyEvents />
          <RoomEvents />
        </div>
      </AuthenticatedTemplate>

      <UnauthenticatedTemplate>
        <p>You are not signed in! Please sign in.</p>
        <SignInButton />
      </UnauthenticatedTemplate>
    </div>
  );
}

export default App;
