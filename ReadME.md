# MS API Testing [Conference Rooms Calendar]

This project was created to test how we can connect and load data from MS Calendar. In the demo app we login and then we have access to load the two calendars.

The structure of the project is pretty simple, only missing thing is a logout button (we will need it in the CRC app).


## Using the project

```
1) npm install OR yarn install
2) npm run dev OR yarn dev
```

## Tech Stack
- React
- TypeScript
- [Vite](https://vitejs.dev/)
- [Styled Components](https://styled-components.com/) (Styling/CSS)
- [React Context API](https://reactjs.org/docs/context.html) (for global state management)
- [React Tsting Library](https://testing-library.com/docs/react-testing-library/intro/) (unit/e2e tests)
- [Vite PWA](https://vite-plugin-pwa.netlify.app/)

## ENV 
Any secrets or configurable variables that we need in our project should be created under a `.env` file. This file is not pushed into the repo. So any value that we have we can add in our MS Channel Wiki tab so anybody can have access to it.



## Code Style

The so called "Code Style" of this project right now is pretty simple - React + TS with support of the basic eslint rules. If you decide you can add other packages like airbnb or extend the current rules. Itwould be very good to not use `any` as much as possible and to create your own types/interfaces.

## Branches, PRs, Code Reviews

### Branch conventions

Everybody should work in branches.

A branch name is created like this: `{feature|fix}/{your name}/cra-{task or story id}-{ ... (name of your branch) }`

- prefix `feature` or `fix`
- your name (probably as in your email)
- Task or Story id (eg. 36)
- Name

```
Example
feature/spetrov/cra-36-setup-docs
```

## PRs and reviews

After a feature is ready we create a PR/pull request (or however the platform is calling this, it varies). Afterwards we have a **code-review** step, where at least 2 people should make a review. In this case I would suggest that the other 2 people of your team should both make a quick code review and someone from Violina or Simeon will make the final check (or even both of them). After the PR is approved it can be merged into your develop branch.


### Branches
We have 2 main branches `develop` & `master`
- `develop` - we merge al of our PRs here and this is more or less our "QA" or "Development" env
- `master` - locked for any feature merges; here we can only create a d2m PR (from `develop` to `master`); this is our "live" env



## Comments
To try to make the project as transparent and easy to work with and understand for any team-mate we want to add comments as a good practice to places where we actually need them. We're using JSDoc to create our comments. We want to add details for any "utility" function that we have. Additionally we can add comments in our components' functions, when needed, but you "don't need" to define comments for the whole component.


## Tests
For better quality we want to write also unit/e2e tests. As far as we see it now using [react-testing-library](https://testing-library.com/docs/react-testing-library/setup) will give us the best experience between writing tests that are close to the usage of the app (e2e) and still having the opportunity to write unit tests (jest).


### What should I test?
- any "utility" function that can be test, like formatting time or transforming objects, but not stuff like fetching data, etc.
- Components - we can test any type of components, even for small things like is the status rendered at all


## Build Tool - Vite
Vite is a pretty cool compiler + build tool, if you decide you can use it for the actual CRC app.
[More on Vite](https://vitejs.dev/guide/). Vite has a pretty big ecosystem that growing every day, here's a list with cool modules and additional packages (can be only for Vite or React specific ones) [Awesome Vite.js](https://github.com/vitejs/awesome-vite#react)



## Useful docs
- [Calendar API](https://docs.microsoft.com/en-us/graph/api/group-list-events?view=graph-rest-1.0&tabs=http) - everything you need for the endpoints
- [Tutorial: Auth Users and fetch MS data](https://docs.microsoft.com/en-us/azure/active-directory/develop/tutorial-v2-react) - the example project was build on top of these examples
- [MS Auth workflow](https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-browser/docs/initialization.md) - we're using MSAL for authenticating our app, this is a good doc/reference point for further configurations
  Additional
- [Quickstart: Sign in and get an access token in a React SPA using the auth code flow](https://docs.microsoft.com/en-us/azure/active-directory/develop/quickstart-v2-javascript-auth-code-react)
- [Get free/busy schedule of users and resources](https://docs.microsoft.com/en-us/graph/outlook-get-free-busy-schedule)
