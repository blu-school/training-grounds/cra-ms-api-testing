import { useMsal } from '@azure/msal-react';
import { useState } from 'react';
import { loadMSData } from '../utils/utils';

const MyEvents = () => {
  const { instance, accounts } = useMsal();
  const [graphData, setGraphData] = useState(null);

  const fetchMyEvents = async () => {
    const url = 'https://graph.microsoft.com/v1.0/me/calendar/events';
    const resp = await loadMSData(instance, accounts, { url });
    setGraphData(resp.value);
  };

  return (
    <div>
      <h2>My events:</h2>
      <button onClick={fetchMyEvents}>Fetch own events</button>
      <div>
        {graphData &&
          graphData.map(function (event, idx) {
            return <li key={idx}>{event.subject}</li>;
          })}
      </div>
    </div>
  );
};

export default MyEvents;
