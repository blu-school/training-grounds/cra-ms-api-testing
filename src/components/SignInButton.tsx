import React from 'react';
import { useMsal } from '@azure/msal-react';
import { loginRequest } from '../config/authConfig';

function handleLogin(instance) {
  instance.loginPopup(loginRequest).catch((e) => {
    console.error(e);
  });
}

/**
 * Renders a button which, when selected, will open a popup for login
 */
const SignInButton = () => {
  const { instance } = useMsal();

  return (
    <button type="button" onClick={() => handleLogin(instance)}>
      Sign in using Popup
    </button>
  );
};
export default SignInButton;
