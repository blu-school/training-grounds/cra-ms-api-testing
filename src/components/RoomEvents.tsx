import { useMsal } from '@azure/msal-react';
import { useState } from 'react';
import { loadMSData } from '../utils/utils';

const RoomEvents = () => {
  const { instance, accounts } = useMsal();
  const [graphData, setGraphData] = useState(null);

  const fetchMyEvents = async () => {
    const url = 'https://graph.microsoft.com/v1.0/users/bottleneck@blubito.com/calendar/events';
    const resp = await loadMSData(instance, accounts, { url });
    setGraphData(resp.value);
  };

  return (
    <div>
      <h2>Bottleneck events:</h2>
      <button onClick={fetchMyEvents}>Fetch room events</button>
      <div>
        {graphData &&
          graphData.map(function (event, idx) {
            return <li key={idx}>{event.subject}</li>;
          })}
      </div>
    </div>
  );
};

export default RoomEvents;
